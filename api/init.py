import os, threading, time, random
import requests
from selenium.common.exceptions import TimeoutException
from webwhatsapi import WhatsAPIDriver
from webwhatsapi.objects.message import Message
from webwhatsapi import WhatsAPIDriverStatus as status

class Whatsapp(object):
    # _ENDPOINT = ''
    def get_endpoint(self):
        try:
            f = open('endpoint.conf', 'r')
            value = f.readline().replace('\n','')
            return value
        except:
            print("error, try again!")

    _PROFILEDIR = os.path.join(".","firefox_cache")
    _SELENIUM_ENV = 'http://ip_vps:4444/wd/hub'

    def __init__(self):
        self._ENDPOINT = self.get_endpoint()
        if not os.path.exists(self._PROFILEDIR):
            os.makedirs(self._PROFILEDIR)
        self.driver = WhatsAPIDriver(
            profile=self._PROFILEDIR,
            client='remote',
            command_executor= self._SELENIUM_ENV #selenium standalone server
        )
        t_start = threading.Thread(target=self.start)
        t_start.start()

    def start(self):
        print('waiting for login')
        while True:
            try:
                filename = random.randint(500,5000)
                self.driver.screenshot('static/%s.png' %filename)
                self.driver.wait_for_login(timeout=10)
            except TimeoutException:
                os.system('rm static/*.png')
            if self.driver.get_status() == 'LoggedIn':
                print('session saved')
                self.driver.save_firefox_profile(remove_old=False)
                break

        os.system('rm static/*.png')
        self.t_wait = threading.Thread(target=self.wait_for_message)
        self.t_wait.start()

    def wait_for_message(self):
        while True:
            print('waiting incoming message')
            for contact in self.driver.get_unread():
                for message in contact.messages:
                    if message.type == 'chat':
                        msg = message.content
                        id = message.chat_id['_serialized']
                        phone = message.chat_id['user']
                        name = message.sender.get_safe_name()
                        timestamp = str(message.timestamp)

                        payload = {"id":id,"phone":phone,"name":name,"message":msg,"timestamp":timestamp}
                        print(payload)
                        r = requests.post(self._ENDPOINT, json=payload)
            if self.driver.get_status() != 'LoggedIn':
                self.start()
            time.sleep(3)
    def send_message(self, id, msg):
        return self.driver.send_message_to_id(id, msg)

if __name__=='__main__':
    app = Whatsapp()
    app.start()
